use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize)]
pub enum Count {
    Inf,
    Finite(u32),
}

pub const PATH: &str = "/tmp/jabbmin.sock";

// #[derive(Debug, Deserialize, Serialize)]
// struct Command {

// }

#[derive(Debug, Deserialize, Serialize)]
pub enum Command {
    Register { username: String, password: String },
    InviteLink { time: u64, users: Count },
    AddAdmin { username: String },
}
