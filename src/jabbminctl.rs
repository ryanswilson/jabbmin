use clap::{Parser, Subcommand};
// use service::{init_tracing, WorldClient};
use std::io::prelude::*;
use std::os::unix::net::UnixStream;
use std::{net::SocketAddr, time::Duration};
// use tracing::Instrument;

#[derive(Subcommand)]
enum Command {
    /// register username pass
    Register {
        ///new users uname
        username: String,

        ///new users password
        password: String,
    },

    /// invite-link time number
    InviteLink {
        ///time in micro seconds, big cringe i know
        time: u64,

        ///number of new users allowed, negative numbers allow infinite, zero , well you can I guess
        users: i32,
    },

    ///upgrade user to admin
    AddAdmin {
        ///username of new admin
        username: String,
    },
}

#[derive(Parser)]
struct Flags {
    /// This is the command to pass to the jabbmin server.
    #[clap(subcommand)]
    command: Command,
}

// support jadminctl socket? register username pass
// 	   jadminctl socket? invite_link_new  time users

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let flags = Flags::parse();

    let comm = match flags.command {
        Command::Register { username, password } => {
            service::Command::Register { username, password }
        }
        Command::InviteLink { time, users } => {
            let count = match users {
                users if users < 0 => service::Count::Inf,
                users if users > 0 => service::Count::Finite(users as u32),
                _ => service::Count::Finite(0),
            };
            service::Command::InviteLink { time, users: count }
        }
        Command::AddAdmin { username } => service::Command::AddAdmin { username },
    };

    let j = serde_json::to_string(&comm)?;
    let js = j.as_bytes();

    let mut stream = UnixStream::connect(service::PATH)?;
    stream.write_all(js)?;
    // let transport = tarpc::serde_transport::tcp::connect(flags.server_addr, Json::default);
    let mut response = String::new();
    stream.read_to_string(&mut response)?;
    println!("{}", response);
    Ok(())
}
