use futures::stream::StreamExt;
use futures::Stream;
use serde_json::Value;
use std::env;
use std::fs;
use std::process::Command;
use std::time::{Duration, SystemTime};
use tokio::io;
use tokio::runtime::Runtime;
use tokio_util::codec::{FramedRead, LinesCodec};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // let server = UnixListener::bind(service::PATH).unwrap();

    let mut rt = Runtime::new()?;

    rt.block_on(async {
        let server = tokio::net::UnixListener::bind(service::PATH).unwrap();
        tokio::spawn(ipc_loop(server));
        tokio::signal::ctrl_c().await.unwrap();
    });

    fs::remove_file(service::PATH).unwrap();
    Ok(())
}

async fn ipc_loop(server: tokio::net::UnixListener) {
    loop {
        let (socket, _) = match server.accept().await {
            Result::Ok(res) => res,
            //is breaking on err correct? TODO log or smth
            Err(_) => break,
        };

        tokio::spawn(async move { run_ipc(socket) });
    }
}

async fn run_ipc(socket: tokio::net::UnixStream) -> () {
    let mut lines = FramedRead::new(socket, LinesCodec::new());

    loop {
        let msg = match lines.next().await {
            Some(msg) => msg,
            None => break,
        };
        match msg {
            Ok(v) => println!("{}", v),
            Err(e) => println!("{}", e),
        }
    }

    // deserialized.map(|x| println!("{}", x));

    // tokio::spawn(async move {
    //     while let Some(msg) = deserial.try_next().await.unwrap() {
    //         println!("GOT: {:?}", msg);
    //     }
    // });
}

fn create_invite(duration: SystemTime, count: service::Count) -> () {}
